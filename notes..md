# App

## entrypoint.sh

- - wait_for_db command halts the execution until we have the database accepting connections
- - uwsgi command:
- - --socket :9000 -> Run the application as a TCP socket on port 9000
- - --workers 4 -> because the application is small and it won't affect the performance. Otherwise it would be preferable to run multiple docker container to handle the application and running just a single or maybe two workers.
- - --master -> runs this as a master service in the terminal meaning that if we pass the exit command to docker then it would shutdown gracefully
- - --enable-threads -> enables multithreading in the application
- - --module app.wsgi -> actual application that uWSGI is going to run.

## Python trivia

- - filter(func, sequence) -> filter function takes in a function to as a check and sequence to run the check on.
- - You can also pass None as the first parameter and then the filter function will filter out all the falsy values. eg. empty list and what not.
- - list.extend(iterative) takes in an iterative and appends it to the list.

## .gitlab-ci.yml

- - It is always best to also run production commands on staging.
- - Jobs are run in serial order as they are written.
- - CI is running in docker and the jobs execute some statement that have to run another docker container. So we use dind service i.e. docker in docker
