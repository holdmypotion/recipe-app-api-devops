terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-holdmypotion"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

# Way to create dynamic variables in terraform. Interpolation Syntax
# concatonate string from different resources
# var is short for variables 
# This would be like prefix = raad-dev
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
